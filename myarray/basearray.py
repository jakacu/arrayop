# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array

import math

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def __add__(self, other):
        if isinstance(other, BaseArray):
            tmp = self.__data[:]
            if other.dtype != self.dtype:
                raise Exception("Matrix types do not match!")
            elif other.shape == self.shape:
                for i in range(len(self.__data)):
                    tmp[i] += other.__data[i]
            else:
                raise Exception("Bad matrix size!")
            return BaseArray(self.shape, dtype=self.dtype, data=tmp)
        else:
            raise Exception("Other type does not match BaseArray")

    def __sub__(self, other):
        if isinstance(other, BaseArray):
            tmp = self.__data[:]
            if other.dtype != self.dtype:
                raise Exception("Matrix types do not match!")
            elif other.shape == self.shape:
                for i in range(len(self.__data)):
                    tmp[i] -= other.__data[i]
            else:
                raise Exception("Bad matrix size!")
            return BaseArray(self.shape, dtype=self.dtype, data=tmp)
        else:
            raise Exception("Other type does not match BaseArray")

    def __mul__(self, other):
        tmp = self.__data[:]
        if isinstance(other, BaseArray):
            if len(self.shape) == 1 and other.shape == self.shape:
                result = 0
                for i in range(len(self.__data)):
                    result += other.__data[i] * self.__data[i]
                return result
            elif len(self.shape) == 2 and other.shape[0] == self.shape[1]:
                tmp = BaseArray((other.shape[1], self.shape[0]), dtype=self.dtype)
                for i in range(1, self.shape[0] + 1):
                    for j in range(1, other.shape[1] + 1):
                        for k in range(1, other.shape[0] + 1):
                            tmp[i, j] += self[i, k] * other[k, j]
                return tmp
            else:
                raise Exception("Bad matrix size!")
        else:
            for i in range(len(self.__data)):
                tmp[i] *= other
        if isinstance(other, float):
            return BaseArray(self.shape, dtype=float, data=tmp)
        return BaseArray(self.shape, dtype=self.dtype, data=tmp)

    def __pow__(self, power):
        if isinstance(power, int):
            if len(self.shape) == 2:
                if self.shape[0] == self.shape[1]:
                    tmp = BaseArray(self.shape, dtype=self.dtype, data=self.__data)
                    for i in range(1, power):
                        tmp *= self
                    return tmp
            raise Exception("Potenciranje matrike zahteva kvadratno matriko")
        raise Exception("Potenca mora biti celo stevilo")

    def __eq__(self, other):
        if self.shape == other.shape and self.dtype == other.dtype:
            for i in range(len(self.__data)):
                if self.__data[i] != other.__data[i]:
                    return False;
            return True;
        else:
            raise Exception("Matrike niso enake velikosti")

    def __truediv__(self, other): #TODO divison
        if isinstance(other, BaseArray):
            raise Exception("Cannot use divison on two matrixes")
        elif isinstance(other, int) or isinstance(other, float):
            return self.__mul__(1/other)
        else:
            raise Exception("Bad divison type, use int or float")

    def __log__(self): #TODO log
        tmp = self.__data[:]
        for i in range(len(self.__data)):
            if tmp[i] == 0:
                tmp[i] = float("-inf")
            elif tmp[i] <= 0:
                tmp[i] = float("nan")
            else:
                tmp[i] = math.log10(tmp[i])
        return BaseArray(self.shape, dtype=float, data=tmp)

    def __exp__(self):
        tmp = []
        for data in self.__data:
            tmp.append(round(math.exp(data), 2))
        return BaseArray(self.shape, dtype=float, data=tmp)

    def print(self):
        arrString = ""
        if len(self.shape) == 1:
            arrString = ' '.join(str(num) for num in self)
        elif len(self.shape) == 2:
            for row in range(1, self.shape[-2] + 1):
                for col in range(1, self.shape[-1] + 1):
                    arrString += '{:6}'.format(self[row, col])
                arrString += "\n"
        elif len(self.shape) == 3:
            for group in range(1, self.shape[-3] + 1):
                for row in range(1, self.shape[-2] + 1):
                    for col in range(1, self.shape[-1] + 1):
                        arrString += '{:6}'.format(self[group, row, col])
                    arrString += "\n"
                arrString += "\n"
        return arrString

    def sort(self, sort_mode="columns"):
        if len(self.shape) == 1:
            merge_sort(self.__data)
        elif len(self.shape) == 2:
            if sort_mode == "rows":
                for i in range(1, self.shape[-2] + 1):
                    tmp = []
                    for j in range(1, self.shape[-1] + 1):
                        tmp.append(self[i, j])
                    merge_sort(tmp)
                    for j in range(1, len(tmp) + 1):
                        self[i, j] = tmp[j-1]
            if sort_mode == "columns":
                for i in range(1, self.shape[-1] + 1):
                    tmp = []
                    for j in range(1, self.shape[-2] + 1):
                        tmp.append(self[j, i])
                    merge_sort(tmp)
                    for j in range(1, len(tmp) + 1):
                        self[j, i] = tmp[j-1]

    def find_element(self, element):
        no_match = 1
        tmp = []
        if len(self.shape) == 1:
            for i in range(1, self.shape[0]+1):
                if self[i] == element:
                    tmp.append("(" + str(i) + ")")
                    no_match = 0
        elif len(self.shape) == 2:
            for i in range(1, self.shape[0]+1):
                for j in range(1, self.shape[1]):
                    if self[i, j] == element:
                        tmp.append("(" + str(i) + ", " + str(j) + ")")
                        no_match = 0
        elif len(self.shape) == 3:
            for i in range(1, self.shape[0]+1):
                for j in range(1, self.shape[1]+1):
                    for k in range(1, self.shape[2]+1):
                        if self[i, j, k] == element:
                            tmp.append("(" + str(i) + ", " + str(j) + ", " + str(k) + ")")
                            no_match = 0
        if no_match == 1:
            tmp = "No matches found";
        return tmp


def merge_sort(alist):
    if len(alist) > 1:
        mid = len(alist) // 2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]
        merge_sort(lefthalf)
        merge_sort(righthalf)
        i = 0
        j = 0
        k = 0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i = i + 1
            else:
                alist[k] = righthalf[j]
                j = j + 1
            k = k + 1
        while i < len(lefthalf):
            alist[k] = lefthalf[i]
            i = i + 1
            k = k + 1
        while j < len(righthalf):
            alist[k] = righthalf[j]
            j = j + 1
            k = k + 1


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True


a = BaseArray((2, 3), dtype=float, data=(1, 0, 2, -1, 3, 1))
b = BaseArray((3, 2), dtype=float, data=(3, 1, 2, 1, 1, 0))
c = a * b

expected_data = BaseArray((2, 2), dtype=float, data=(5.0, 1.0, 4.0, 2.0))
if expected_data == c:
    g = 0