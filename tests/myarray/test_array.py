from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
import math


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    #moji testi:

    def test_array_to_string_1D_integer(self):
        a = BaseArray((4, ), dtype=int, data=(11, 0, -2, 1))
        expected = "11 0 -2 1"
        self.assertEqual(expected, a.print())

    def test_array_to_string_1D_float(self):
        a = BaseArray((4, ), dtype=float, data=(3.41, 0.2, -2.4, 1.1))
        expected = "3.41 0.2 -2.4 1.1"
        self.assertEqual(expected, a.print())

    def test_array_to_string_2D_integer(self):
        a = BaseArray((2, 2), dtype=int, data=(1, 0, -2, 1))
        expected = "     1     0\n    -2     1\n"
        self.assertEqual(expected, a.print())

    def test_array_to_string_2D_float(self):
        a = BaseArray((2, 2), dtype=float, data=(3.41, 0.2, -2.41, 1.1))
        expected = "  3.41   0.2\n -2.41   1.1\n"
        self.assertEqual(expected, a.print())

    def test_array_to_string_3D_integer(self):
        a = BaseArray((2, 2, 3), dtype=int, data=(3, 0, -2, 4, 1, 3, 0, -2, 1, 4, 2, 4))
        expected = "     3     0    -2\n     4     1     3\n\n     0    -2     1\n     4     2     4\n\n"
        self.assertEqual(expected, a.print())

    def test_array_to_string_3D_float(self):
        a = BaseArray((2, 2, 3), dtype=float, data=(3.42, 0.2, -2.4, 4.1, 1.1, 3.4, 0.2, -2.4, 1.1, 4.1, 2.1, 4.3))
        expected = "  3.42   0.2  -2.4\n   4.1   1.1   3.4\n\n   0.2  -2.4   1.1\n   4.1   2.1   4.3\n\n"
        self.assertEqual(expected, a.print(), "3d float to string error")

    def test_array_find_element_1D_nomatches(self):
        a = BaseArray((4,), dtype=int, data=(1, 0, -2, 1))
        matches = a.find_element(3)
        expected = "No matches found"
        self.assertEqual(expected, matches)

    def test_array_find_element_1D_1match(self):
        a = BaseArray((4,), dtype=int, data=(1, 0, -2, 1))
        matches = a.find_element(0)
        expected = ["(2)"]
        self.assertEqual(expected, matches)

    def test_array_find_element_1D_multiplematches(self):
        a = BaseArray((4,), dtype=int, data=(1, 0, -2, 1))
        matches = a.find_element(1)
        expected = ["(1)", "(4)"]
        self.assertEqual(expected, matches)

    def test_array_find_element_2D_nomatches(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, -2, 1, 5, 2))
        matches = a.find_element(3)
        expected = "No matches found"
        self.assertEqual(expected, matches)

    def test_array_find_element_2D_1match(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, -2, 1, 5, 2))
        matches = a.find_element(0)
        expected = ["(1, 2)"]
        self.assertEqual(expected, matches)

    def test_array_find_element_2D_multiplematches(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, -2, 1, 1, 2))
        matches = a.find_element(1)
        expected = ["(1, 1)", "(2, 1)", "(2, 2)"]
        self.assertEqual(expected, matches)

    def test_array_find_element_3D_nomatches(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, -2, 1, 5, 2, 7, 8))
        matches = a.find_element(3)
        expected = "No matches found"
        self.assertEqual(expected, matches)

    def test_array_find_element_3D_1match(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, -2, 1, 5, 2, 7, 8))
        matches = a.find_element(0)
        expected = ["(1, 1, 2)"]
        self.assertEqual(expected, matches)

    def test_array_find_element_3D_multiplematches(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, -2, 1, 1, 2, 1, 8))
        matches = a.find_element(1)
        expected = ["(1, 1, 1)", "(1, 2, 2)", "(2, 1, 1)", "(2, 2, 1)"]
        self.assertEqual(expected, matches)

    def test_merge_sort_1d_integer(self):
        a = BaseArray((4,), dtype=int, data=(1, 0, -2, 1))
        a.sort()
        expected_data = BaseArray((4,), dtype=int, data=(-2, 0, 1, 1))
        self.assertEqual(expected_data == a, True)

    def test_merge_sort_1d_float(self):
        a = BaseArray((4,), dtype=float, data=(1., 0., -2., 1.))
        a.sort()
        expected_data = BaseArray((4,), dtype=float, data=(-2., 0., 1., 1.))
        self.assertEqual(expected_data == a, True)

    def test_merge_sort_2d_row_integer(self):
        a = BaseArray((2, 2), dtype=int, data=(1, 0, 1, -1))
        a.sort("rows")
        expected_data = BaseArray((2, 2), dtype=int, data=(0, 1, -1, 1))
        self.assertEqual(expected_data == a, True)

    def test_merge_sort_2d_row_float(self):
        a = BaseArray((2, 2), dtype=float, data=(1., 0., 1., -1.))
        a.sort("rows")
        expected_data = BaseArray((2, 2), dtype=float, data=(0., 1., -1., 1.))
        self.assertEqual(expected_data == a, True)

    def test_merge_sort_2d_column_integer(self):
        a = BaseArray((2, 2), dtype=int, data=(1, 0, 1, -1))
        a.sort("columns")
        expected_data = BaseArray((2, 2), dtype=int, data=(1, -1, 1, 0))
        self.assertEqual(expected_data == a, True)

    def test_merge_sort_2d_column_float(self):
        a = BaseArray((2, 2), dtype=float, data=(1., 0., 1., -1.))
        a.sort("columns")
        expected_data = BaseArray((2, 2), dtype=float, data=(1., -1., 1., 0.))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_1D_int(self):
        a = BaseArray((4, ), dtype=int, data=(1, 0, 1, -1))
        a = a * 3
        expected_data = BaseArray((4, ), dtype=int, data=(3, 0, 3, -3))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_2D_int(self):
        a = BaseArray((2, 2), dtype=int, data=(1, 0, 1, -1))
        a = a * 3
        expected_data = BaseArray((2, 2), dtype=int, data=(3, 0, 3, -3))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_1D_float(self):
        a = BaseArray((4, ), dtype=float, data=(1., 0., 1., -1.))
        a = a * 3.0
        expected_data = BaseArray((4, ), dtype=float, data=(3., 0., 3., -3.))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_2D_float(self):
        a = BaseArray((2, 2), dtype=float, data=(1., 0., 1., -1.))
        a = a * 3.0
        expected_data = BaseArray((2, 2), dtype=float, data=(3., 0., 3., -3.))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_3D_int(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, 1, -1, 1, 1, 2, 3))
        a = a * 3
        expected_data = BaseArray((2, 2, 2), dtype=int, data=(3, 0, 3, -3, 3, 3, 6, 9))
        self.assertEqual(expected_data == a, True)

    def test_mat_scalar_mul_3D_float(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(1., 0., 1., -1., 1.0, 1.0, 2.0, 3.0))
        a = a * 3.0
        expected_data = BaseArray((2, 2, 2), dtype=float, data=(3., 0., 3., -3., 3., 3., 6., 9.))
        self.assertEqual(expected_data == a, True)

    def test_mat_mul_2D_int(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((3, 2), dtype=int, data=(3, 1, 2, 1, 1, 0))
        c = a * b
        expected_data = BaseArray((2, 2), dtype=int, data=(5, 1, 4, 2))
        self.assertEqual(expected_data == c, True)

    def test_mat_mul_2D_float(self):
        a = BaseArray((2, 3), dtype=float, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((3, 2), dtype=float, data=(3, 1, 2, 1, 1, 0))
        c = a * b

        expected_data = BaseArray((2, 2), dtype=float, data=(5.0, 1.0, 4.0, 2.0))
        self.assertEqual(expected_data == c, True)

    def test_mat_mul_wrong_size(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        with self.assertRaises(Exception):
            a*b

    def test_mat_div_1D_int(self):
        a = BaseArray((6, ), dtype=int, data=(1, 0, 2, -1, 3, 1))
        c = a / 2
        expected_data = BaseArray((6, ), dtype=float, data=(1/2, 0, 2/2, -1/2, 3/2, 1/2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_2D_int(self):
        a = BaseArray((2,3), dtype=int, data=(1, 0, 2, -1, 3, 1))
        c = a / 2
        expected_data = BaseArray((2,3), dtype=float, data=(1 / 2, 0, 2 / 2, -1 / 2, 3 / 2, 1 / 2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_1D_float(self):
        a = BaseArray((6, ), dtype=float, data=(1.0, 0.0, 2.0, -1.0, 3.0, 1.0))
        c = a / 2.0
        expected_data = BaseArray((6, ), dtype=float, data=(1/2, 0, 2/2, -1/2, 3/2, 1/2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_2D_float(self):
        a = BaseArray((2, 3), dtype=float, data=(1.0, 0.0, 2.0, -1.0, 3.0, 1.0))
        c = a / 2.0
        expected_data = BaseArray((2, 3), dtype=float, data=(1/2, 0, 2/2, -1/2, 3/2, 1/2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_3D_int(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, 2, -1, 3, 1, 1, 1))
        c = a / 2
        expected_data = BaseArray((2, 2, 2), dtype=float, data=(1/2, 0, 2/2, -1/2, 3/2, 1/2, 1/2, 1/2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_3D_float(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(1.0, 0.0, 2.0, -1.0, 3.0, 1.0, 1.0, 1.0))
        c = a / 2.0
        expected_data = BaseArray((2, 2, 2), dtype=float, data=(1 / 2, 0, 2 / 2, -1 / 2, 3 / 2, 1 / 2, 1 / 2, 1 / 2))
        self.assertEqual(expected_data == c, True)

    def test_mat_div_wrong_type(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 0, 2, -1, 3, 1, 5, 1, 2))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        with self.assertRaises(Exception):
            a/b

    def test_mat_add_1D_int(self):
        a = BaseArray((6, ), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((6, ), dtype=int, data=(3, 1, 2, 1, 1, 0))
        c = a + b
        expected_data = BaseArray((6, ), dtype=int, data=(4, 1, 4, 0, 4, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_1D_float(self):
        a = BaseArray((6, ), dtype=float, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((6, ), dtype=float, data=(3, 1, 2, 1, 1, 0))
        c = a + b
        expected_data = BaseArray((6, ), dtype=float, data=(4, 1, 4, 0, 4, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_2D_int(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        c = a + b
        expected_data = BaseArray((2, 3), dtype=int, data=(4, 1, 4, 0, 4, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_2D_float(self):
        a = BaseArray((2, 3), dtype=float, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((2, 3), dtype=float, data=(3, 1, 2, 1, 1, 0))
        c = a + b
        expected_data = BaseArray((2, 3), dtype=float, data=(4, 1, 4, 0, 4, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_3D_int(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, 2, -1, 3, 1, 1, 1))
        b = BaseArray((2, 2, 2), dtype=int, data=(3, 1, 2, 1, 1, 0, 1, 1))
        c = a + b
        expected_data = BaseArray((2, 2, 2), dtype=int, data=(4, 1, 4, 0, 4, 1, 2, 2))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_3D_float(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(1, 0, 2, -1, 3, 1, 1, 1))
        b = BaseArray((2, 2, 2), dtype=float, data=(3, 1, 2, 1, 1, 0, 1, 1))
        c = a + b
        expected_data = BaseArray((2, 2, 2), dtype=float, data=(4, 1, 4, 0, 4, 1, 2, 2))
        self.assertEqual(expected_data == c, True)

    def test_mat_add_wrong_size(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 0, 2, -1, 3, 1, 5, 1, 2))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        with self.assertRaises(Exception):
            a+b

    def test_mat_add_number_exception(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 0, 2, -1, 3, 1, 5, 1, 2))
        with self.assertRaises(Exception):
            a+5

    def test_mat_sub_1D_int(self):
        a = BaseArray((6, ), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((6, ), dtype=int, data=(3, 1, 2, 1, 1, 0))
        c = a - b
        expected_data = BaseArray((6, ), dtype=int, data=(-2, -1, 0, -2, 2, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_1D_float(self):
        a = BaseArray((6, ), dtype=float, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((6, ), dtype=float, data=(3, 1, 2, 1, 1, 0))
        c = a - b
        expected_data = BaseArray((6, ), dtype=float, data=(-2, -1, 0, -2, 2, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_2D_int(self):
        a = BaseArray((2, 3), dtype=int, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        c = a - b
        expected_data = BaseArray((2, 3), dtype=int, data=(-2, -1, 0, -2, 2, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_2D_float(self):
        a = BaseArray((2, 3), dtype=float, data=(1, 0, 2, -1, 3, 1))
        b = BaseArray((2, 3), dtype=float, data=(3, 1, 2, 1, 1, 0))
        c = a - b
        expected_data = BaseArray((2, 3), dtype=float, data=(-2, -1, 0, -2, 2, 1))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_3D_int(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 0, 2, -1, 3, 1, 1, 1))
        b = BaseArray((2, 2, 2), dtype=int, data=(3, 1, 2, 1, 1, 0, 1, 1))
        c = a - b
        expected_data = BaseArray((2, 2, 2), dtype=int, data=(-2, -1, 0, -2, 2, 1, 0, 0))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_3D_float(self):
        a = BaseArray((2, 2, 2), dtype=float, data=(1, 0, 2, -1, 3, 1, 1, 1))
        b = BaseArray((2, 2, 2), dtype=float, data=(3, 1, 2, 1, 1, 0, 1, 1))
        c = a - b
        expected_data = BaseArray((2, 2, 2), dtype=float, data=(-2, -1, 0, -2, 2, 1, 0, 0))
        self.assertEqual(expected_data == c, True)

    def test_mat_sub_wrong_size(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 0, 2, -1, 3, 1, 5, 1, 2))
        b = BaseArray((2, 3), dtype=int, data=(3, 1, 2, 1, 1, 0))
        with self.assertRaises(Exception):
            a - b

    def test_mat_sub_number_exception(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 0, 2, -1, 3, 1, 5, 1, 2))
        with self.assertRaises(Exception):
            a - 5

    def test_mat_pow_int(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 2, 3, 4, 1, -2, 4))
        a = a.__pow__(3)
        expected = BaseArray((3, 3), dtype=int, data=(35, -24, 124, 56, -29, 192, -12, -56, -1))
        self.assertEqual(a == expected, True)

    def test_mat_pow_float(self):
        a = BaseArray((3, 3), dtype=float, data=(1.0, 2.0, 3.0, 2.0, 3.0, 4.0, 1.0, -2.0, 4.0))
        a = a.__pow__(3)
        expected = BaseArray((3, 3), dtype=float, data=(35.0, -24.0, 124.0, 56.0, -29.0, 192.0, -12.0, -56.0, -1.0))
        self.assertEqual(a == expected, True)

    def test_mat_pow_wrong_size(self):
        a = BaseArray((3, 2), dtype=int, data=(1, 2, 3, 2, 3, 4))
        with self.assertRaises(Exception):
            a = a.__pow__(3)

    def test_mat_pow_float_exponent_exception(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 2, 3, 4, 6, 4, 1))
        with self.assertRaises(Exception):
            a = a.__pow__(3.6)

    def test_mat_log_1D(self):
        a = BaseArray((9, ), dtype=int, data=(1, 2, 3, 2, 3, 4, 6, 4, 1))
        a = a.__log__()
        log = BaseArray((9, ), dtype=float, data=(0, math.log10(2), math.log10(3), math.log10(2), math.log10(3),
                                                   math.log10(4), math.log10(6), math.log10(4), 0))
        self.assertEqual(a == log, True)

    def test_mat_log_2D(self):
        a = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 2, 3, 4, 6, 4, 1))
        a = a.__log__()
        log = BaseArray((3, 3), dtype=float, data=(0, math.log10(2), math.log10(3), math.log10(2), math.log10(3),
                                                   math.log10(4), math.log10(6), math.log10(4), 0))
        self.assertEqual(a == log, True)

    def test_mat_log_3D(self):
        a = BaseArray((2, 2, 2), dtype=int, data=(1, 2, 3, 2, 3, 4, 6, 4))
        a = a.__log__()
        log = BaseArray((2, 2, 2), dtype=float, data=(0, math.log10(2), math.log10(3), math.log10(2), math.log10(3),
                                                   math.log10(4), math.log10(6), math.log10(4)))
        self.assertEqual(a == log, True)

    '''
    def test_mat_log_negative_numbers(self):
        a = BaseArray((3, 3), dtype=int, data=(0, -2, -3, -2, -3, -4, -6, 0, 0))
        a = a.__log__()
        log = BaseArray((3, 3), dtype=float, data=(float("-inf"), float("nan"), float("nan"), float("nan"), float("nan"), float("nan"), float("nan"), float("-inf"), float("-inf")))
        self.assertEqual(a == log, True)
'''
    def test_mat_1D_exp(self):
        a = BaseArray((4, ), dtype=int, data=(0, -2, -3, -2))
        a = a.__exp__()
        expected = BaseArray((4,), dtype=float, data=(round(math.exp(0), 2), round(math.exp(-2), 2), round(math.exp(-3), 2),
                                                      round(math.exp(-2), 2)))
        self.assertEqual(a == expected, True)

    def test_mat_2D_exp(self):
        a = BaseArray((2, 2), dtype=int, data=(0, -2, -3, -2))
        a = a.__exp__()
        expected = BaseArray((2, 2), dtype=float, data=(round(math.exp(0), 2), round(math.exp(-2), 2), round(math.exp(-3), 2),
                                                      round(math.exp(-2), 2)))
        self.assertEqual(a == expected, True)
